import pymysql

global conn,cursor
conn = pymysql.connect(host='az1-sr3.supercp.com', user='adpr_dare_dev', passwd='D4O,zhE%$r*z',
                       db='adpr_dare_dev', charset='utf8')
cursor = conn.cursor()


yelp_insert_scrape_data = "INSERT INTO `scrape_data` (`batch_id`, `platform_id`, `platform_name`, `batch_date`, `business_name`, `telephone`, `business_page`, `category`, `website`, `review_count`, `street`, `locality`, `state`, `region`, `address`, `zipcode`, `listing_url`, `star_rating`, `years_business`,`record_status`, `created_date`, `updated_date`, `created_by`, `updated_by` ) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s ,%s)"

yelp_insert_review_remark_detail = "INSERT INTO `review_remark_detail` (`record_id`, `review_remark`, `review_rating_overall`, `review_date`, `review_by`, `review_rating_expertise`, `review_rating_professional`, `review_rating_promptness`, `like_count`, `like_useful`,`like_funny`,`like_cool`, `is_reviewer_picture_available`, `is_picture_video_available_in_review`, `is_from_trusted_platform`, `composit_score`,`review_status`,`created_date`, `updated_date`, `created_by`, `updated_by` ) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s,%s)"

yelp_update_scrape_data = "UPDATE `scrape_data` SET `category`=%s,`street`=%s,`locality`=%s, `state`=%s, `telephone`=%s, `website`=%s, `address`=%s, `region`=%s, `zipcode`=%s WHERE `record_id`=%s"

yelp_platform_master ="SELECT * FROM platform_master where platform_name=%s"

yelp_insert_scrape_summary_data = "INSERT INTO `scrape_execution_summary` (`batch_id`, `batch_status`, `search_criteria`,`extracted_records`,`pages`, `keyword`, `place`, `created_date`, `updated_date`, `created_by`, `updated_by`, `execution_time`) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s,%s)"