#!/usr/bin/env python
# -*- coding: utf-8 -*-
import re

import null
import requests
from lxml import html

import sys
import pymysql
from datetime import date
import datetime



review_record_count = 0
business_record_count = 0
address = ''
region = ''
website = ''
category = ''
telephone = ''
zipcode = ''
batch_staus = ''


def parse_listing(keyword, place, page, batchId):

    global business_detaills_review
    global business_record_count,review_record_count,address,zipcode,region,website,category,telephone,batch_staus



    if page > 1:
        url = "https://www.yelp.com/search?find_desc={0}&find_loc={1}&ns=1&start={2}".format(keyword, place,
                                                                                             (page - 1) * 10)
    else:
        url = "https://www.yelp.com/search?find_desc={0}&find_loc={1}&ns=1".format(keyword, place)


    # print("business_url ",url)

    username = 'spbd0655c5'
    password = 'Dar3T3am927'


    proxy_rorating_business = f'http://{username}:{password}@us.smartproxy.com:10000'
    # print("proxy_rorating",proxy_rorating_business)

    headers = {'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8',
               'Accept-Encoding': 'gzip, deflate, br',
               'Accept-Language': 'en-GB,en;q=0.9,en-US;q=0.8,ml;q=0.7',
               'Cache-Control': 'max-age=0',
               'Connection': 'keep-alive',
               'Host': 'www.yelp.com',
               'Upgrade-Insecure-Requests': '1',
               'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/64.0.3282.140 Safari/537.36 OPR/66.0.3515.36'
               }

    try:
        response = requests.get(url, verify = False, proxies={'http': proxy_rorating_business, 'https': proxy_rorating_business})
        # print("proxy_rorating_business",proxy_rorating_business)
        if response.status_code == 200:
            parser = html.fromstring(response.text)
            # making links absolute
            base_url = "https://www.yelp.com"
            parser.make_links_absolute(base_url)

            XPATH_LISTINGS = "//div[@class='lemon--div__373c0__1mboc border-color--default__373c0__3-ifU']//ul[@class='lemon--ul__373c0__1_cxs undefined list__373c0__2G8oH']//li[@class='lemon--li__373c0__1r9wz border-color--default__373c0__3-ifU']//div[contains(@class,'lemon--div__373c0__1mboc container__373c0__3HMKB hoverable__373c0__VqkG7 margin-t3__373c0__1l90z margin-b3__373c0__q1DuY padding-t3__373c0__1gw9E padding-r3__373c0__57InZ padding-b3__373c0__342DA padding-l3__373c0__1scQ0 border--top__373c0__3gXLy border--right__373c0__1n3Iv border--bottom__373c0__3qNtD border--left__373c0__d1B7K border-color--default__373c0__3-ifU')]"
            listings = parser.xpath(XPATH_LISTINGS)

            single_digits = ["0","0.5", "1","1.5", "2","2.5", "3","3.5",
                             "4", "4.5","5", "6", "7",
                             "8", "9"];


            for results in listings[1:]:
                if (business_record_count == int(no_of_records_to_scrape)):
                    if (business_record_count == int(no_of_records_to_scrape)) and (business_record_count == review_record_count):
                         batch_staus = "success"
                    elif (business_record_count == int(no_of_records_to_scrape)) or (business_record_count < review_record_count):
                        batch_staus = "partial"
                    else:
                        batch_staus = "error"
                    print(batch_staus)
                    return  batch_staus
                try:
                    XPATH_BUSINESS_NAME = ".//h4[@class='lemon--h4__373c0__1yd__ heading--h4__373c0__27bDo alternate__373c0__2Mge5']//a[@class='lemon--a__373c0__IEZFH link__373c0__1G70M link-color--inherit__373c0__3dzpk link-size--inherit__373c0__1VFlE']//text()"
                    XPATH_BUSSINESS_PAGE = ".//a[@class='lemon--a__373c0__IEZFH link__373c0__1G70M link-color--inherit__373c0__3dzpk link-size--inherit__373c0__1VFlE']//@href"
                    XPATH_RANK = ".//div[@class='lemon--div__373c0__1mboc border-color--default__373c0__3-ifU']//h4//span[@class='lemon--span__373c0__3997G text__373c0__26Xrb text-color--black-regular__373c0__B5jQ9 text-align--left__373c0__Rrl_f text-weight--bold__373c0__20M7i text-size--inherit__373c0__dzW7L']/text()"
                    XPATH_RATING = ".//div[@class='lemon--div__373c0__1mboc border-color--default__373c0__3-ifU']//div[@class='lemon--div__373c0__1mboc attribute__373c0__1hPI_ display--inline-block__373c0__1ZKqC border-color--default__373c0__3-ifU']//span[@class='lemon--span__373c0__3997G text__373c0__2Kxyz reviewCount__373c0__2r4xT text-color--black-extra-light__373c0__2OyzO text-align--left__373c0__2XGa-']//text()"
                    XPATH_RATING_STAR = ".//div[contains(@aria-label,'star rating')]/@aria-label"

                    raw_business_name = results.xpath(XPATH_BUSINESS_NAME)
                    raw_business_page = results.xpath(XPATH_BUSSINESS_PAGE)
                    raw_rank = results.xpath(XPATH_RANK)
                    raw_rating = results.xpath(XPATH_RATING)
                    raw_star_rating = results.xpath(XPATH_RATING_STAR)

                    business_name = ''.join(raw_business_name).strip() if raw_business_name else None
                    # print("Business name:::",business_name)
                    if (searchBy == 'business'):

                        if business_name == keyword.replace("%", " "):
                            ismatch = True
                        else:
                            if any(element in business_name for element in (keyword.replace("%", " ").split(" "))):
                                if phoneParam != null:
                                    if telephone == phoneParam:
                                        ismatch = True
                                elif zipcode != null:
                                    if zipcode == zipcodeParam:
                                        ismatch = True
                                else:
                                    ismatch = False
                            else:
                                ismatch = False
                    else:
                        ismatch = True

                    if ismatch:
                        business_page = ''.join(raw_business_page[0]).strip() if raw_business_page[0] else None
                        # telephone = ''.join(raw_business_telephone).strip() if raw_business_telephone else None
                        rank = ''.join(raw_rank).replace('.\xa0', '') if raw_rank else None
                        review_count = ''.join(raw_rating).replace("reviews", "").replace("review", "").strip() if raw_rating else None
                        review_count_int = int(0) if review_count is None else int(review_count)
                        if raw_star_rating:
                            star_rating = ''.join(raw_star_rating).strip() if raw_star_rating else None
                            star_rating = star_rating.replace('star rating', '')
                        else:
                            star_rating = 0

                        business_detaills_review = ''
                        business_detaills_review_by = ''
                        business_detaills_review_date = ''
                        business_detaills_review_contains_image = ''
                        is_reviewer_picture_available = ''


                        conn = pymysql.connect(host='az1-sr3.supercp.com', user='adpr_dare_dev', passwd='D4O,zhE%$r*z',
                                               db='adpr_dare_dev', charset='utf8')
                        cursor = conn.cursor()
                        cursor.execute("SELECT * FROM platform_master where platform_name=%s", hostname)

                        myresult = cursor.fetchone()
                        insert_scrape_data = "INSERT INTO `scrape_data` (`batch_id`, `platform_id`, `platform_name`, `batch_date`, `business_name`, `telephone`, `business_page`, `category`, `website`, `review_count`, `street`, `locality`, `state`, `region`, `address`, `zipcode`, `listing_url`, `star_rating`, `years_business`,`record_status`, `created_date`, `updated_date`, `created_by`, `updated_by` ) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s )"
                        cursor.execute(insert_scrape_data, (
                            batchId, myresult[0], hostname, str(date.today()), business_name, telephone,
                            business_page, '',
                            '', review_count, '', '', '',
                            '', address, '', response.url,
                            star_rating, '', '', str(datetime.datetime.utcnow()), str(datetime.datetime.utcnow()),
                            'python', 'python'))
                        last_record_id = cursor.lastrowid
                        # print("last_record_id:::", last_record_id)
                        conn.commit()
                        business_record_count +=1
                        


                        review_page_range = int(review_count_int)//20
                        if (review_page_range == 0) or (review_page_range < int(review_count_int)/20):
                            review_page_range = review_page_range +1

                        for pagination in range(int(review_page_range)):

                            pagination = pagination + 1
                            if pagination > 1:
                                url2 = "{0}&start={1}".format(
                                    business_page,
                                    (pagination - 1) * 20)
                            else:
                                url2 = "{0}".format(business_page)
                            # print("retrieving remark ", url2)

                            # url2 = "https://www.yelp.com/biz/la-top-roofing-los-angeles-2?osq=Roofing%20Contractor&start=100"
                            # print("retrieving remark ", url2)

                            # proxy_random = int(random.randint(10001, 29999))
                            # print("proxy_random", proxy_random)
                            #
                            # proxy_sticky = f'http://{username}:{password}@us.smartproxy.com:{proxy_random}'
                            proxy_rorating_remarks = f'http://{username}:{password}@us.smartproxy.com:10000'

                            business_page_response = requests.get(url2, verify=False, proxies={'http': proxy_rorating_remarks, 'https': proxy_rorating_remarks})
                            # print("remark rotating proxy",proxy_rorating_remarks)
                            if business_page_response.status_code == 200:
                                address, telephone, zipcode = extract_review(
                                    business_page_response, conn, cursor, last_record_id)

                            else:
                                proxy_rorating_remarks_rety = f'http://{username}:{password}@us.smartproxy.com:10000'

                                business_page_response = requests.get(url2, verify=False,
                                                                      proxies={'http': proxy_rorating_remarks_rety,
                                                                               'https': proxy_rorating_remarks_rety})
                                if business_page_response.status_code == 200:
                                    address, telephone, zipcode = extract_review(
                                        business_page_response, conn, cursor, last_record_id)
                                else:
                                    print("Problem in details loading page after second attempt")

                        review_record_count += 1
                        # print("review_record_count*********$$$$$$$$", review_record_count)

                except Exception as e:
                    # print(e)
                    continue

            return "success"


        elif response.status_code == 404:
            print("Could not find a location matching", place)
        else:
            print("Failed to process page1")
            return []

    except Exception as e:
        # print(e)
        return "error"


def extract_review(business_page_response, conn, cursor, last_record_id):
    global business_detaills_review
    details_parser = html.fromstring(business_page_response.text)
    details_base_url = "https://www.yelp.com"
    details_parser.make_links_absolute(details_base_url)
    try:
        telephone = details_parser.xpath(
            "//div[@class='lemon--div__373c0__1mboc island-section__373c0__3vKXy border--top__373c0__19Owr border-color--default__373c0__2oFDT']//div[@class='lemon--div__373c0__1mboc arrange__373c0__UHqhV gutter-12__373c0__3kguh vertical-align-middle__373c0__2TQsQ border-color--default__373c0__2oFDT']//div[contains(@class,'lemon--div__373c0__1mboc arrange-unit__373c0__1piwO arrange-unit-fill__373c0__17z0h border-color--default__373c0__2oFDT')]//p[@class='lemon--p__373c0__3Qnnj text__373c0__2pB8f text-color--normal__373c0__K_MKN text-align--left__373c0__2pnx_']//text()")
        telephone = ''.join(telephone).strip() if telephone else None

        address = details_parser.xpath(
            "//div[@class='lemon--div__373c0__1mboc arrange__373c0__UHqhV gutter-30__373c0__2PiuS border-color--default__373c0__2oFDT']//div[@class='lemon--div__373c0__1mboc arrange-unit__373c0__1piwO arrange-unit-fill__373c0__17z0h border-color--default__373c0__2oFDT']//address[@class='lemon--address__373c0__2sPac']//p//span/text()")
        address = ''.join(address).strip() if address else None

        # street = details_parser.xpath("//*[@id='wrap']/div[3]/div/div[1]/div[2]/div/div/div[2]/div[1]/section/div[2]/div[1]/div/div/div/div[1]/address/p[1]/span/text()")
        # street = ''.join(street).strip() if street else None

        # city = details_parser.xpath(
        #     "//*[@id='wrap']/div[3]/div/div[1]/div[2]/div/div/div[2]/div[1]/section/div[2]/div[1]/div/div/div/div["
        #     "1]/address/p[2]/span/text()")
        # city = ''.join(city).strip() if city else None
        # city  = re.sub(r',[^,]*$', '', city)


        zipcode1 = address[-5:]
        zipcode = ''.join(zipcode1).strip() if zipcode1 else None

        region = details_parser.xpath(
            "//div[@class='lemon--div__373c0__1mboc arrange-unit__373c0__1piwO arrange-unit-fill__373c0__17z0h border-color--default__373c0__2oFDT']//div[@class='lemon--div__373c0__1mboc border-color--default__373c0__2oFDT']//p[@class='lemon--p__373c0__3Qnnj text__373c0__2pB8f text-color--normal__373c0__K_MKN text-align--left__373c0__2pnx_']/text()")
        region = ''.join(region[0]).strip() if region else None

        website = details_parser.xpath(
            "//div[@class='lemon--div__373c0__1mboc stickySidebar__373c0__3PY1o border-color--default__373c0__2oFDT']//section[contains(@class,'lemon--section__373c0__fNwDM')]//div[@class='lemon--div__373c0__1mboc island__373c0__3fs6U u-padding-t1 u-padding-r1 u-padding-b1 u-padding-l1 border--top__373c0__19Owr border--right__373c0__22AHO border--bottom__373c0__uPbXS border--left__373c0__1SjJs border-color--default__373c0__2oFDT background-color--white__373c0__GVEnp']/div[@class='lemon--div__373c0__1mboc island-section__373c0__3vKXy border--top__373c0__19Owr border-color--default__373c0__2oFDT']//div/div[@class='lemon--div__373c0__1mboc arrange-unit__373c0__1piwO arrange-unit-fill__373c0__17z0h border-color--default__373c0__2oFDT']//a//text()")
        website = ''.join(website[0]).strip() if website else None

        category = details_parser.xpath(
            "//div[@class='lemon--div__373c0__1mboc border-color--default__373c0__2oFDT']//span[@class='lemon--span__373c0__3997G display--inline__373c0__1DbOG u-space-r1 border-color--default__373c0__2oFDT']//span[contains(@class,'lemon--span__373c0__3997G text__373c0__2pB8f text-color--normal__373c0__K_MKN text-align--left__373c0__2pnx_ text-size--large__373c0__1568g')]//a//text()")
        category = ''.join(category).strip() if category else None

        update_scrape_data = "UPDATE `scrape_data` SET `category`=%s, `telephone`=%s, `website`=%s, `address`=%s, `region`=%s, `zipcode`=%s WHERE `record_id`=%s"
        cursor.execute(update_scrape_data, (category, telephone, website, address, region, zipcode, last_record_id))
        conn.commit()
    except Exception as e:
        print(e)

    BUSINESS_PAGE_XPATH_LISTINGS = "//section[@class='lemon--section__373c0__fNwDM u-space-t4 u-padding-t4 border--top__373c0__19Owr border-color--default__373c0__2oFDT']//div//section[@class='lemon--section__373c0__fNwDM u-space-t4 u-padding-t4 border--top__373c0__19Owr border-color--default__373c0__2oFDT']//div[@class='lemon--div__373c0__1mboc spinner-container__373c0__N6Hff border-color--default__373c0__2oFDT']//div[@class='lemon--div__373c0__1mboc border-color--default__373c0__2oFDT']//ul[@class='lemon--ul__373c0__1_cxs undefined list__373c0__2G8oH']/li"
    business_page_detaills = details_parser.xpath(BUSINESS_PAGE_XPATH_LISTINGS)
    listing_useful_count2 = []
    listing_funny_count2 = []
    listing_cool_count2 = []
    i = 0

    for detaills_results in business_page_detaills:
        try:
            XPATH_BUSINESS_DETAILS_REVIEW_BY = "//*[@id='wrap']/div[3]/div/div[1]/div[2]/div/div/div[2]/div[1]/section/div/section[2]/div[2]/div/ul/li[" + str(
                i + 1) + "]/div/div[1]/div[1]/div/div/div[2]/div[1]/a/span/text()"
            raw_business_detaills_review_by = detaills_results.xpath(XPATH_BUSINESS_DETAILS_REVIEW_BY)
            # print("raw_business_detaills_review_by",raw_business_detaills_review_by)
            business_detaills_review_by = raw_business_detaills_review_by
            # print("business_detaills_review_by",business_detaills_review_by)

            XPATH_BUSINESS_DETAILS_REVIEW_DATE = "//*[@id='wrap']/div[3]/div/div[1]/div[2]/div/div/div[2]/div[1]/section/div/section[2]/div[2]/div/ul/li[" + str(
                i + 1) + "]/div/div[2]/div[1]/div/div[2]/span//text()"
            raw_business_detaills_review_date = detaills_results.xpath(XPATH_BUSINESS_DETAILS_REVIEW_DATE)
            business_detaills_review_date = raw_business_detaills_review_date
            # print("business_detaills_review_date",business_detaills_review_date)

            XPATH_BUSINESS_DETAILS_REVIEW = ".//div[@class='lemon--div__373c0__1mboc u-space-b2 border-color--default__373c0__2oFDT']//p//span//text()"
            raw_business_detaills_review = detaills_results.xpath(XPATH_BUSINESS_DETAILS_REVIEW)
            # print("raw_business_detaills_review",raw_business_detaills_review)
            business_detaills_review = ''.join(raw_business_detaills_review)
            # print("business_detaills_review",business_detaills_review)

            XPATH_IS_REVIEWER_PICTURE_AVAILABLE = ".//div[@class='lemon--div__373c0__1mboc on-click-container border-color--default__373c0__2oFDT']//a//img/@src"
            raw_business_is_reviewer_pic_available = detaills_results.xpath(
                XPATH_IS_REVIEWER_PICTURE_AVAILABLE)
            is_reviewer_picture_available = raw_business_is_reviewer_pic_available
            # print("is_reviewer_picture_available",is_reviewer_picture_available)

            XPATH_BUSINESS_DETAILS_REVIEW_IMAGE = ".//div[@class='lemon--div__373c0__1mboc arrange-unit__373c0__1piwO arrange-unit-grid-column--8__373c0__2yTAx border-color--default__373c0__2oFDT']//div[contains(@class,'lemon--div__373c0__1mboc u-space-b2 border-color--default__373c0__2oFDT')]//div[contains(@class,'lemon--div__373c0__1mboc display--inline-block__373c0__2de_K u-space-r2 u-space-b-half border-color--default__373c0__2oFDT')]//a//p/text()"
            raw_business_details_review_contains_image = detaills_results.xpath(XPATH_BUSINESS_DETAILS_REVIEW_IMAGE)

            if raw_business_details_review_contains_image == []:
                business_detaills_review_contains_image = '0'
            else:
                business_detaills_review_contains_image = '1'

            # print("business_detaills_review_contains_image",business_detaills_review_contains_image)

            XPATH_LISTINGS_useful_count = "//*[@id='wrap']/div[3]/div/div[1]/div[2]/div/div/div[2]/div[1]/section/div/section[2]/div[2]/div/ul/li[" + str(
                i + 1) + "]/div/div[2]/div/div[1]/span[1]/button/span/span[2]/span/span/text()"
            listing_useful_count = detaills_results.xpath(XPATH_LISTINGS_useful_count)
            if len(listing_useful_count) == 0:
                listing_useful_count2 = '0'
            else:
                listing_useful_count2 = listing_useful_count[1:]
            # print("listing_useful_count2",listing_useful_count2)


            XPATH_LISTINGS_funny_count = "//*[@id='wrap']/div[3]/div/div[1]/div[2]/div/div/div[2]/div[1]/section/div/section[2]/div[2]/div/ul/li[" + str(
                i + 1) + "]/div/div[2]/div/div[1]/span[2]/button/span/span[2]/span/span/text()"
            listing_funny_count = detaills_results.xpath(XPATH_LISTINGS_funny_count)
            if len(listing_funny_count) == 0:
                listing_funny_count2 = '0'
            else:
                listing_funny_count2 = listing_funny_count[1:]
            # print("listing_funny_count2",listing_funny_count2)

            XPATH_LISTINGS_cool_count = "//*[@id='wrap']/div[3]/div/div[1]/div[2]/div/div/div[2]/div[1]/section/div/section[2]/div[2]/div/ul/li[" + str(
                i + 1) + "]/div/div[2]/div/div[1]/span[3]/button/span/span[2]/span/span/text()"
            listing_cool_count = detaills_results.xpath(XPATH_LISTINGS_cool_count)
            if len(listing_cool_count) == 0:
                listing_cool_count2 = '0'
            else:
                listing_cool_count2 = listing_cool_count[1:]
            # print("listing_cool_count2",listing_cool_count2)
            i = i + 1
            # @@@@@@@@@@ SCRAPE REVIEW REMARK DETAIL TABLE INSERTION@@@@@@@@@@@@@@@

            insert_review_remark_detail = "INSERT INTO `review_remark_detail` (`record_id`, `review_remark`, `review_rating_overall`, `review_date`, `review_by`, `review_rating_expertise`, `review_rating_professional`, `review_rating_promptness`, `like_count`, `like_useful`,`like_funny`,`like_cool`, `is_reviewer_picture_available`, `is_picture_video_available_in_review`, `is_from_trusted_platform`, `composit_score`, `review_status`, `created_date`, `updated_date`, `created_by`, `updated_by` ) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)"
            for index in range(len(business_detaills_review_by)):
                try:
                    total_like = int(listing_useful_count2[index]) + int(listing_funny_count2[index]) + int(
                        listing_cool_count2[index])
                    image = "0" if raw_business_is_reviewer_pic_available[
                                       index] == 'https://s3-media0.fl.yelpcdn.com/assets/srv0/yelp_styleguide/514f6997a318/assets/img/default_avatars/user_60_square.png' else "1"
                    business_detaills_review1 = business_detaills_review
                    cursor.execute(insert_review_remark_detail, (
                        last_record_id,
                        business_detaills_review1, '',
                        business_detaills_review_date[index],
                        business_detaills_review_by[index], '', '', '',
                        total_like, listing_useful_count2[index], listing_funny_count2[index],
                        listing_cool_count2[index],
                        image,
                        business_detaills_review_contains_image, '1',
                        '', '', str(datetime.datetime.utcnow()), str(datetime.datetime.utcnow()), 'python', 'python'))
                except Exception as e:
                    # print(e)
                    continue
                conn.commit()


        except Exception as e:
            # print(e)
            continue

    return address, telephone, zipcode




if __name__ == "__main__":
    start = datetime.datetime.utcnow()
    # print("start time:", start)

    # logging.basicConfig(filename="C:\Dare\python\yelp.log",
    #                     format='%(asctime)s %(message)s',
    #                     filemode='w',
    #                     level=logging.DEBUG)
    # # Creating an object
    # logger = logging.getLogger()
    # # Setting the threshold of logger to DEBUG
    # logger.setLevel(logging.DEBUG)

    hostname = sys.argv[1]
    searchBy = sys.argv[2]
    keyword = sys.argv[3]
    place = sys.argv[4]
    pages = sys.argv[5]


    if pages.__contains__(','):
        pages = pages.split(',')
        no_of_pages= pages[0]
        no_of_records_to_scrape = pages[1]
    else:
        # print("Error getting per page result,Invalid input")
        raise Exception('Error getting per page result,Invalid input for : {}'.format(pages))


    if (searchBy == 'business'):
        phoneParam = sys.argv[6]
        zipcodeParam = sys.argv[7]
    else:
        phoneParam = null
        zipcodeParam = null

    batchId = sys.argv[-1]


    for page in range(int(no_of_pages)):

        page = page + 1
        # print("==================================================")
        # print("Loop", page)
        # print("==================================================")
        status = parse_listing(keyword, place, page, batchId)
    execution_time = str(datetime.datetime.utcnow() - start)
    # print("Time Taken: ", execution_time)

    # if review_record_count == 0:
    #     batch_status = "Error"
    # elif review_record_count == (pages*10):
    #     batch_status = "Success"
    # else:
    #     batch_status = "partial Success"

    conn_status = pymysql.connect(host='az1-sr3.supercp.com', user='adpr_dare_dev', passwd='D4O,zhE%$r*z',
                           db='adpr_dare_dev', charset='utf8')
    cursor = conn_status.cursor()
    insert_scrape_data = "INSERT INTO `scrape_execution_summary` (`batch_id`, `batch_status`, `search_criteria`, `pages`, `keyword`, `place`, `created_date`, `updated_date`, `created_by`, `updated_by`, `execution_time`) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s )"
    cursor.execute(insert_scrape_data, (
        batchId, status, '',no_of_pages, keyword, place, datetime.datetime.utcnow(), datetime.datetime.utcnow(),
        'program', 'program', execution_time))
    conn_status.commit()
