#!/usr/bin/env python
# -*- coding: utf-8 -*-
import os
import re

import null
import requests
from geotext import GeoText
from lxml import html
import multiprocessing
import credential as c
import urls as u
import x_path_listings as x

import pickle
import db as d
import sys
import pymysql
from multiprocessing import Pool
from datetime import date

import datetime
import logging

from globals import Globals


review_record_count = 0
business_record_count = 0
address = ''
region = ''
website = ''
category = ''
telephone1 = ''
zipcode = ''
batch_status = ''




def parse_listing(keyword, place, page, batchId):



    global business_detaills_review,hostname,no_of_pages,no_of_records_to_scrape,phoneParam,zipcodeParam
    global review_record_count,business_record_count,address,zipcode,region,website,category,telephone1,search_criteria_url,batch_status

    if (searchBy == 'business'):
        search_criteria_url = (
                    hostname + ' ' + "business" + ' ' + keyword + ' ' + place + ' ' + no_of_pages + ' ' + no_of_records_to_scrape + ' ' + phoneParam + ' ' + zipcodeParam + ' ' + batchId)
    else:
        search_criteria_url = (
                    hostname + ' ' + "keyword" + ' ' + keyword + ' ' + place + ' ' + no_of_pages + ' ' + no_of_records_to_scrape + ' ' + batchId)




    if page > 1:
        url = u.yelp_url.format(keyword, place,(page - 1) * 10)

    else:
        url = u.yelp_url_else.format(keyword, place)






    try:
        response = requests.get(url, verify=False,proxies={'http': c.proxy_rorating_business, 'https': c.proxy_rorating_business})
        logging.info("proxy_rorating_business="+' '+c.proxy_rorating_business)
        if response.status_code == 200:
            parser = html.fromstring(response.text)


            parser.make_links_absolute(u.yelp_base_url)

            XPATH_LISTINGS =x.yelp_XPATH_LISTINGS
            listings = parser.xpath(XPATH_LISTINGS)



            for results in listings[1:]:
                if (business_record_count == int(no_of_records_to_scrape)):
                    if (business_record_count == int(no_of_records_to_scrape)) and (
                            business_record_count == review_record_count):
                        batch_status = "success"
                    elif (business_record_count == int(no_of_records_to_scrape)) or (
                            business_record_count < review_record_count):
                        batch_status = "partial"
                    else:
                        batch_status = "error"
                    print(batch_status)
                    return batch_status

                try:
                    XPATH_BUSINESS_NAME =x.yelp_XPATH_BUSINESS_NAME
                    XPATH_BUSSINESS_PAGE = x.yelp_XPATH_BUSSINESS_PAGE
                    XPATH_RANK =x.yelp_XPATH_RANK
                    XPATH_RATING =x.yelp_XPATH_RATING
                    XPATH_RATING_STAR = x.yelp_XPATH_RATING_STAR

                    raw_business_name = results.xpath(XPATH_BUSINESS_NAME)
                    raw_business_page = results.xpath(XPATH_BUSSINESS_PAGE)
                    raw_rank = results.xpath(XPATH_RANK)
                    raw_rating = results.xpath(XPATH_RATING)
                    raw_star_rating = results.xpath(XPATH_RATING_STAR)

                    business_name = ''.join(raw_business_name).strip() if raw_business_name else None

                    # logging.info("Business name="+'  '+business_name)
                    if (searchBy == 'business'):

                        if business_name == keyword.replace("%", " "):
                            ismatch = True
                        else:
                            if any(element in business_name for element in (keyword.replace("%", " ").split(" "))):
                                if phoneParam != null:
                                    if telephone1 == phoneParam:
                                        ismatch = True
                                elif zipcode != null:
                                    if zipcode == zipcodeParam:
                                        ismatch = True
                                else:
                                    ismatch = False
                            else:
                                ismatch = False
                    else:
                        ismatch = True

                    if ismatch:
                        business_page = ''.join(raw_business_page[0]).strip() if raw_business_page[0] else None

                        rank = ''.join(raw_rank).replace('.\xa0', '') if raw_rank else None
                        review_count = ''.join(raw_rating).replace("reviews", "").replace("review", "").strip() if raw_rating else None
                        logging.info("Review Count=",review_count)

                        review_count_int = int(0) if review_count is None else int(review_count)

                        logging.info("Review count=",review_count_int)
                        if raw_star_rating:
                            star_rating = ''.join(raw_star_rating).strip() if raw_star_rating else None
                            star_rating = star_rating.replace('star rating', '')
                        else:
                            star_rating = 0

                        business_detaills_review = ''
                        business_detaills_review_by = ''
                        business_detaills_review_date = ''
                        business_detaills_review_contains_image = ''
                        is_reviewer_picture_available = ''

                        conn = d.conn
                        cursor = conn.cursor()
                        cursor.execute(d.yelp_platform_master,hostname)

                        myresult = cursor.fetchone()




                        cursor.execute(d.yelp_insert_scrape_data, (
                            batchId, myresult[0], hostname, str(date.today()), business_name, telephone1,
                            business_page, '',
                            '', review_count, '', '', '',
                            '', address, '', response.url,
                            star_rating, '','', str(datetime.datetime.utcnow()), str(datetime.datetime.utcnow()),
                            'python', 'python'))
                        last_record_id = cursor.lastrowid
                        # logging.info("Last record id=",last_record_id)


                        conn.commit()
                        business_record_count += 1
                        print("businesss reocrd count",business_record_count)



                        review_page_range = int(review_count_int)//20
                        if (review_page_range == 0) or (review_page_range < int(review_count_int)/20):
                            review_page_range = review_page_range +1

                        for pagination in range(int(review_page_range)):

                            pagination = pagination + 1
                            if pagination > 1:
                                url2 = "{0}&start={1}".format(
                                    business_page,
                                    (pagination - 1) * 20)
                            else:
                                url2 = "{0}".format(business_page)
                            logging.info("retrieving remark ="+' '+url2)

                            # url2 = "https://www.yelp.com/biz/la-top-roofing-los-angeles-2?osq=Roofing%20Contractor&start=100"


                            # proxy_random = int(random.randint(10001, 29999))

                            #
                            # proxy_sticky = f'http://{username}:{password}@us.smartproxy.com:{proxy_random}'


                            business_page_response = requests.get(url2, verify=False, proxies={'http': c.proxy_rorating_remarks, 'https': c.proxy_rorating_remarks})
                            # logging.info("remark rotating proxy="+ ' '+proxy_rorating_remarks)
                            if business_page_response.status_code == 200:
                                address, telephone1, zipcode = extract_review(
                                    business_page_response, conn, cursor, last_record_id)

                            else:


                                business_page_response = requests.get(url2, verify=False,
                                                                      proxies={'http': c.proxy_rorating_remarks_rety,
                                                                               'https': c.proxy_rorating_remarks_rety})
                                if business_page_response.status_code == 200:
                                    address, telephone1, zipcode = extract_review(
                                        business_page_response, conn, cursor, last_record_id)
                                else:
                                    logging.error("Problem in details loading page after second attempt")

                        review_record_count += 1
                        print("review record count",review_record_count)

                        logging.info("business_record_count=",business_record_count)

                except Exception as e:
                    logging.exception(e)

                    continue
            return "success"



        elif response.status_code == 404:

            logging.warning("Could not find a location matching="+' '+ place)
        else:
            logging.error("Failed to process page1")
            return []

    except Exception as e:
        logging.exception(e)
        return "Error"


def extract_review(business_page_response,conn,cursor,last_record_id):
    global business_detaills_review
    details_parser = html.fromstring(business_page_response.text)

    details_parser.make_links_absolute(u.yelp_details_base_url)

    try:
        telephone1 = details_parser.xpath(
            x.yelp_telephone)
        telephone1 = ''.join(telephone1).strip() if telephone1 else None

        re_telephone=re.findall("[(][\d]{3}[)][ ]?[\d]{3}-[\d]{4}", telephone1)

        telephone1= str(re_telephone)[2:-2]


        address = details_parser.xpath(x.yelp_address )
        address = ''.join(address).strip() if address else None


        zipcode1 = address[-5:]
        zipcode = ''.join(zipcode1).strip() if zipcode1 else None

        state = address.rsplit(',', 1)[1].strip(zipcode)

        street_abbr = ['Ave', 'Blvd', 'Bldg', 'Crt', 'Cres', 'Dr', 'Pl', 'Rd', 'Sq', 'Stn', 'St', 'Terr']
        for i in street_abbr:
            if address.__contains__(i):
                address1 = address.replace(i, '')
                places = GeoText(address1)
                city = ''.join(places.cities) if places else None
            else:
                places = GeoText(address)
                city = ''.join(places.cities) if places else None

        # places = GeoText(address)
        # city = ''.join(places.cities) if places else None


        street = address.replace(state, '').replace(str(city), '').replace(zipcode, '').replace(',', '')

        region = details_parser.xpath(x.yelp_region)
        region = ''.join(region[0]).strip() if region else None

        website = details_parser.xpath(x.yelp_website)
        website = ''.join(website[0]).strip() if website else None

        category = details_parser.xpath(x.yelp_category)
        category = ','.join(category).strip() if category else None

        cursor.execute(d.yelp_update_scrape_data, (category, street, city, state, telephone1, website, address, region, zipcode, last_record_id))
        conn.commit()


    except Exception as e:
        logging.exception(e)


    BUSINESS_PAGE_XPATH_LISTINGS =x.yelp_BUSINESS_PAGE_XPATH_LISTINGS

    business_page_detaills = details_parser.xpath(BUSINESS_PAGE_XPATH_LISTINGS)
    listing_useful_count2 = []
    listing_funny_count2 = []
    listing_cool_count2 = []
    i = 0

    for detaills_results in business_page_detaills:
        try:

            XPATH_BUSINESS_DETAILS_REVIEW_BY = "//*[@id='wrap']/div[3]/div/div[1]/div[2]/div/div/div[2]/div[1]/section/div/section[2]/div[2]/div/ul/li[" + str(
                i + 1) + "]/div/div[1]/div[1]/div/div/div[2]/div[1]/a/span/text()"
            raw_business_detaills_review_by = detaills_results.xpath(XPATH_BUSINESS_DETAILS_REVIEW_BY)

            business_detaills_review_by = raw_business_detaills_review_by


            XPATH_BUSINESS_DETAILS_REVIEW_DATE = "//*[@id='wrap']/div[3]/div/div[1]/div[2]/div/div/div[2]/div[1]/section/div/section[2]/div[2]/div/ul/li[" + str(
                i + 1) + "]/div/div[2]/div[1]/div/div[2]/span//text()"
            raw_business_detaills_review_date = detaills_results.xpath(XPATH_BUSINESS_DETAILS_REVIEW_DATE)
            business_detaills_review_date = raw_business_detaills_review_date


            XPATH_BUSINESS_DETAILS_REVIEW = ".//div[@class='lemon--div__373c0__1mboc u-space-b2 border-color--default__373c0__2oFDT']//p//span//text()"
            raw_business_detaills_review = detaills_results.xpath(XPATH_BUSINESS_DETAILS_REVIEW)

            business_detaills_review = ''.join(raw_business_detaills_review)


            XPATH_IS_REVIEWER_PICTURE_AVAILABLE = ".//div[@class='lemon--div__373c0__1mboc on-click-container border-color--default__373c0__2oFDT']//a//img/@src"
            raw_business_is_reviewer_pic_available = detaills_results.xpath(
                XPATH_IS_REVIEWER_PICTURE_AVAILABLE)
            is_reviewer_picture_available = raw_business_is_reviewer_pic_available


            XPATH_BUSINESS_DETAILS_REVIEW_IMAGE = ".//div[@class='lemon--div__373c0__1mboc arrange-unit__373c0__1piwO arrange-unit-grid-column--8__373c0__2yTAx border-color--default__373c0__2oFDT']//div[contains(@class,'lemon--div__373c0__1mboc u-space-b2 border-color--default__373c0__2oFDT')]//div[contains(@class,'lemon--div__373c0__1mboc display--inline-block__373c0__2de_K u-space-r2 u-space-b-half border-color--default__373c0__2oFDT')]//a//p/text()"
            raw_business_details_review_contains_image = detaills_results.xpath(XPATH_BUSINESS_DETAILS_REVIEW_IMAGE)

            if raw_business_details_review_contains_image == []:
                business_detaills_review_contains_image = '0'
            else:
                business_detaills_review_contains_image = '1'


            XPATH_LISTINGS_useful_count = "//*[@id='wrap']/div[3]/div/div[1]/div[2]/div/div/div[2]/div[1]/section/div/section[2]/div[2]/div/ul/li[" + str(
                i + 1) + "]/div/div[2]/div/div[1]/span[1]/button/span/span[2]/span/span/text()"
            listing_useful_count = detaills_results.xpath(XPATH_LISTINGS_useful_count)
            if len(listing_useful_count) == 0:
                listing_useful_count2 = '0'
            else:
                listing_useful_count2 = listing_useful_count[1:]



            XPATH_LISTINGS_funny_count = "//*[@id='wrap']/div[3]/div/div[1]/div[2]/div/div/div[2]/div[1]/section/div/section[2]/div[2]/div/ul/li[" + str(
                i + 1) + "]/div/div[2]/div/div[1]/span[2]/button/span/span[2]/span/span/text()"
            listing_funny_count = detaills_results.xpath(XPATH_LISTINGS_funny_count)
            if len(listing_funny_count) == 0:
                listing_funny_count2 = '0'
            else:
                listing_funny_count2 = listing_funny_count[1:]


            XPATH_LISTINGS_cool_count = "//*[@id='wrap']/div[3]/div/div[1]/div[2]/div/div/div[2]/div[1]/section/div/section[2]/div[2]/div/ul/li[" + str(
                i + 1) + "]/div/div[2]/div/div[1]/span[3]/button/span/span[2]/span/span/text()"
            listing_cool_count = detaills_results.xpath(XPATH_LISTINGS_cool_count)
            if len(listing_cool_count) == 0:
                listing_cool_count2 = '0'
            else:
                listing_cool_count2 = listing_cool_count[1:]

            i = i + 1
            # @@@@@@@@@@ SCRAPE REVIEW REMARK DETAIL TABLE INSERTION@@@@@@@@@@@@@@@


            for index in range(len(business_detaills_review_by)):
                try:
                    total_like = int(listing_useful_count2[index]) + int(listing_funny_count2[index]) + int(
                            listing_cool_count2[index])
                    image = "0" if raw_business_is_reviewer_pic_available[
                                       index] == 'https://s3-media0.fl.yelpcdn.com/assets/srv0/yelp_styleguide/514f6997a318/assets/img/default_avatars/user_60_square.png' else "1"
                    business_detaills_review1 = business_detaills_review
                    cursor.execute(d.yelp_insert_review_remark_detail, (
                        last_record_id,
                        business_detaills_review1, '',
                        business_detaills_review_date[index],
                        business_detaills_review_by[index], '', '', '','',
                        total_like, listing_useful_count2[index], listing_funny_count2[index], listing_cool_count2[index],
                        image,
                        business_detaills_review_contains_image, '1',
                        '', str(datetime.datetime.utcnow()), str(datetime.datetime.utcnow()), 'python', 'python'))
                except Exception as e:

                    continue
                conn.commit()


        except Exception as e:

            continue

    return address, telephone1, zipcode


if __name__ == "__main__":
    start = datetime.datetime.utcnow()

    logging.basicConfig(filename="yelp.log",
                        format='%(asctime)s,%(msecs)d %(levelname)-8s [%(filename)s:%(lineno)d] %(message)s',
                        datefmt='%Y-%m-%d:%H:%M:%S',
                        level=logging.DEBUG, filemode='a+', )

    hostname = sys.argv[1]
    searchBy = sys.argv[2]
    keyword = sys.argv[3]
    place = sys.argv[4]
    pages = sys.argv[5]


    # perPageResult = sys.argv[-2]
    if pages.__contains__(','):
        pages = pages.split(',')
        no_of_pages = pages[0]
        no_of_records_to_scrape = pages[1]
    else:
        logging.error("Error getting per page result,Invalid input")
        raise Exception('Error getting per page result,Invalid input for : {}'.format(pages))

    if (searchBy == 'business'):
        phoneParam = sys.argv[6]
        zipcodeParam = sys.argv[7]
    else:
        phoneParam = null
        zipcodeParam = null

    batchId = sys.argv[-1]

    issuccess = False
    # manager = multiprocessing.Manager()
    # processes = manager.list()
    # processes =[]
    for page in range(int(no_of_pages)):
        page = page + 1

        status = parse_listing(keyword, place, page, batchId)

    execution_time = str(datetime.datetime.utcnow() - start)

    logging.info("Time Taken: " + execution_time)



    lconn = d.conn
    lcursor = lconn.cursor()

    lcursor.execute(d.yelp_insert_scrape_summary_data, (
        batchId, status,search_criteria_url,business_record_count, no_of_pages, keyword, place, str(datetime.datetime.utcnow()), str(datetime.datetime.utcnow()),
        'program', 'program', execution_time))
    lconn.commit()










